BEGIN;

CREATE TABLE clubindex (
	club_id		serial NOT NULL,
	name		varchar(60) NOT NULL,
	address		varchar(60),
	homepage	varchar(150),
	phone		varchar(30),
	created		timestamp NOT NULL,
	changed		timestamp NOT NULL,
	CONSTRAINT clubindex_pkey PRIMARY KEY (club_id));

CREATE OR REPLACE FUNCTION trigger_func_clubindex()
	RETURNS TRIGGER AS '
DECLARE
	t timestamp;
BEGIN
	t := localtimestamp;
	IF (TG_OP = ''INSERT'') THEN
		IF (NEW.created NOTNULL OR NEW.changed NOTNULL) THEN
			RAISE EXCEPTION ''The fields "created" and "changed" can be filled only by the trigger.'';
		END IF;
		NEW.created = t;
		NEW.changed = t;
	ELSEIF (TG_OP = ''UPDATE'') THEN
		IF (OLD.created <> NEW.created OR OLD.changed <> NEW.changed) THEN
			RAISE EXCEPTION ''The fields "created" and "changed" can be modified only by the trigger.'';
		END IF;
		NEW.changed = t;
	END IF;
	RETURN NEW;
END;
' LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER trigger_clubindex
	BEFORE INSERT OR UPDATE
	ON clubindex
	FOR EACH ROW
	EXECUTE PROCEDURE trigger_func_clubindex();

COMMIT;
