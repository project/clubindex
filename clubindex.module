<?php

function _t($english, $german)
{
	global $locale;
	return $locale == 'de' ? $german : t($english);
}

function clubindex_set_title()
{
	drupal_set_title(_t('Club Index', 'Vereinsverzeichnis'));
}

function clubindex_help($section='')
{
	$output = '';

	switch ($section)
	{
		case "admin/modules#description" :
			$output = _t("Manages a database of sport clubs.",
				     "Verwaltet eine Datenbank von Vereinen.");
			break;
	}

	return $output;
}

function clubindex_perm()
{
	return array('edit database');
}

function clubindex_menu()
{
	$items = array();
	$arg_1 = arg(1);

	$items[] = array('path' => 'clubindex',
			 'callback' => 'clubindex_all',
			 'access' => TRUE);
	
	$items[] = array('path' => 'clubindex/list',
			 'title' => t('list'),
			 'access' => TRUE,
			 'type' => MENU_DEFAULT_LOCAL_TASK);

	$items[] = array('path' => is_numeric($arg_1) ?
				"clubindex/$arg_1/edit" : 'clubindex/edit',
			 'title' => _t('Edit / Add', 'Bearbeiten / Hinzufügen'),
			 'callback' => 'clubindex_edit',
			 'weight' => 1,
			 'access' => user_access('edit database'),
			 'type' => MENU_LOCAL_TASK);

	$items[] = array('path' => "clubindex/$arg_1/delete",
			 'title' => t('delete'),
			 'callback' => 'clubindex_delete',
			 'access' => user_access('edit database'),
			 'type' => MENU_CALLBACK);

	return $items;
}

function clubindex_all()
{
	$editable = user_access('edit database');
	
	$header = array(
		array('data'  => _t('Club', 'Verein'),
		      'field' => 'name',
		      'sort'  => 'asc'),
		array('data'  => _t('Location', 'Trainigsst&auml;tte'),
		      'field' => 'address'),
		array('data'  => _t('Homepage / Phone', 'Homepage / Tel.'))
		);
		
	if ($editable)
	{
		$header[] = ' ';
		$header[] = ' ';
	}

	$query = 'SELECT club_id, name, address, homepage, phone ' .
		 'FROM {clubindex}' . tablesort_sql($header);
	if (!($queryResult = pager_query($query, 50)))
		$err_msg = _t('Database query failed. Module-Table possibly ' .
			      'not yet created.',
			      'Datenbankabfrage fehlgeschlagen. Modul-' .
			      'Tabelle möglicherweise noch nicht angelegt.');
	
	while ($club = db_fetch_object($queryResult))
	{
		$row = array('data' =>
			array(
				$club->name,
				$club->address,
				$club->homepage != '' ?
					l($club->homepage, $club->homepage) :
					$club->phone
			)
		);

		if ($editable)
		{
			$row['data'][] = l(t('edit'), 'clubindex/' .
				$club->club_id . '/edit');
			$row['data'][] = l(t('delete'), 'clubindex/' .
				$club->club_id . '/delete');
		}
		
		$rows[] = $row;
	}

	if (!$rows)
	{
		if(!isset($err_msg))
			$err_msg = _t('There are no clubs in the database',
			'Es sind keine Vereine in der Datenbank vorhanden.');
			
		$rows[] = array(
			array(
				'data' => $err_msg,
				'colspan' => $editable ? '5' : '3')
		);
	}

	menu_rebuild();
	clubindex_set_title();

	$pager = theme('pager', NULL, 50, 0);
	if (!empty($pager))
		$rows[] = array(
			array(
				'data' => $pager,
				'colspan' => $editable ? '5' : '3')
		);

	$page_content = '<br/>' . theme("table", $header, $rows);
	
	print theme("page", $page_content);
}

function clubindex_edit()
{
	$name = $_POST['edit']['club'];
	if (isset($name))
	{
		$name	   = trim($name);
		$address   = $_POST['edit']['address'];
		$homepage  = $_POST['edit']['homepage'];
		$phone	   = $_POST['edit']['phone'];
		$timestamp = time();
		
		if ($name == '')
		{
			$msg = _t(
				'You have to enter the name of the club.',
				'Sie m&uuml;ssen einen Namen f&uuml;r ' .
					'den Verein angeben.');
		}
		else if (is_numeric(arg(1)))
		{
			$query = "UPDATE {clubindex} SET name='$name', " .
				 "address='$address', homepage='$homepage', " .
				 "phone='$phone', changed=$timestamp " .
				 'WHERE club_id = ' . arg(1);

			if (db_query($query))
				$msg = _t(
				'Club was edited, successfully.',
				'Verein wurde erfolgreich bearbeitet.');
			else
				$msg = _t(
				'Changes couldn\'t become saved.',
			'&Auml;nderungen konnten nicht gespeichert werden.');
		}
		else
		{
			$query = 'INSERT INTO {clubindex} ' .
				 '(name, address, homepage, phone, created, ' .
				 "changed) VALUES ('$name', '$address', " .
				 "'$homepage', '$phone', $timestamp, " .
				 $timestamp . ')';

			if (db_query($query))
				$msg = _t(
				'Club was added successfully to the database.',
				'Verein wurde erfolgreich der Datenbank ' .
					'hinzugef&uuml;gt.');
			else
				$msg = _t(
				'Club couldn\'t become added to the database.',
				'Verein konnte der Datenbank nicht ' .
					'hinzugef&uuml;gt werden.');
		}
	}
	else if (is_numeric(arg(1)))
	{
		$query = 'SELECT name, address, homepage, phone ' .
			 'FROM {clubindex} WHERE club_id = ' . arg(1);

		if ($club = db_fetch_object(db_query($query)))
		{
			$name = $club->name;
			$address = $club->address;
			$homepage = $club->homepage;
			$phone = $club->phone;
		}
		else
			$msg = _t(
			'The selected club couldn\'t become found.',
			'Der ausgew&auml;hlte Verein konnte nicht gefunden ' .
				'werden.');
	}

	$form['club'] = array(
		'#type' => 'textfield',
		'#title' => _t('Club', 'Verein'),
		'#default_value' => $name,
		'#size' => 30,
		'#maxlength' => 60,
		'#required' => TRUE
	);

	$form['address'] = array(
		'#type' => 'textfield',
		'#title' => _t('Location', 'Trainigsst&auml;tte'),
		'#default_value' => $address,
		'#size' => 30,
		'#maxlength' => 60
	);

	$form['homepage'] = array(
		'#type' => 'textfield',
		'#title' => _t('Homepage', 'Homepage'),
		'#default_value' => $homepage,
		'#size' => 60,
		'#maxlength' => 150
	);

	$form['phone'] = array(
		'#type' => 'textfield',
		'#title' => _t('Phone', 'Telefon'),
		'#default_value' => $phone,
		'#size' => 30,
		'#maxlength' => 30
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => _t('Save', 'Speichern')
	);

	menu_rebuild();
	clubindex_set_title();

	$page_content = drupal_get_form('clubindex_page', $form);

	if (isset($msg))
		$page_content .= '<br/><br/>' . $msg;

	print theme("page", $page_content);
}

function clubindex_delete()
{
	$club_id = arg(1);

	if (is_numeric($club_id))
	{
		$query_sel = 'SELECT name FROM {clubindex} WHERE club_id = ' .
			     $club_id;
		$query_del = 'DELETE FROM {clubindex} WHERE club_id = ' .
			     $club_id;

		$club = db_fetch_object(db_query($query_sel));
		if ($club && db_query($query_del))
		{
			$club_name = $club->name;
			$page_content .= _t('The Club "' . $club_name .
				   '" became deleted, successfully.',
				   'Der Verein "' . $club_name .
				   '" wurde erfolgreich gel&ouml;scht.');
		}
		else
		{
			$page_content .= _t('The selected club couldn\'t ' .
				   'become deleted from the database.',
				   'Der ausgew&auml;hlte Verein konnte nicht ' .
				   'aus der Datenbank gel&ouml;scht werden.');
		}
	}
	else
	{
		$page_content .= '"' . $club_id . '" ' .
				_t('is no valid id.',
				   'ist keine g&uuml;ltige id.');
	}

	$page_content = '<br/>' . $page_content;

	clubindex_set_title();
	
	print theme("page", $page_content);
}

?>
